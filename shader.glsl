//extern number time;

vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 pixel_coords) {

    float tx = love_ScreenSize.x/3;
    if (pixel_coords.x < tx) {
        return vec4(0, 0, 0, 1);
    } else if (pixel_coords.x > tx && pixel_coords.x < tx*2) {
        return vec4(1, 1, 1, 1);
    } else {
        return vec4(0, 0, 0, 1);
    }
}